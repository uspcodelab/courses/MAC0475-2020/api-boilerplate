import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { v4 as uuid } from 'uuid';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.disable('x-powered-by');

export async function setupApp({ db, broker }) {
  app.post('/people', (req, res) => {
    const person = {
      name: req.body.name,
      uuid: uuid(),
    };

    broker.publish('people', JSON.stringify(person));

    res.json({ person });
  });

  app.get('/people', async (req, res) => {
    const people = await db.collection('people').find({}).toArray();
    res.json({ people });
  });
}
